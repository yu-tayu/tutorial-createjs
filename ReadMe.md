# CreateJS チュートリアル

## 導入編

- [CreateJSとは / CreateJSのサンプル](docs/basic.md)
- [簡単なサンプルを試そう](docs/quickstart.md)
- [制作の前にブラウザの開発ツールの使い方を抑えよう](docs/debug.md)

## 表示オブジェクト

- [表示オブジェクト](docs/displayobject.md)
- シェイプ
  - [線と塗り](docs/shape_fill_stroke.md)
  - [様々な色の設定方法](docs/shape_color.md)
  - [様々な図形の描画](docs/shape_draw.md)
- [テキスト](docs/text.md)
- [画像の表示](docs/bitmap.md)
- [表示オブジェクトのグループ化](docs/nest.md)

## モーション

- [Ticker の使い方](docs/ticker.md)
- [Tween の使い方](docs/tween.md)

## インタラクション

- マウス操作
	- [マウス座標](docs/mouse_xy.md)
	- [クリックイベント](docs/mouse_click.md)
	- [マウスオーバー](docs/mouse_over.md)
  - [ドラッグ&ドロップ](docs/mouse_drag.md)
  - [タッチデバイス対応](docs/mouse_touch.md)
- キーボード操作
  - [キーボードイベントの基本](docs/keyboard_basic.md)
  - [キーボードで自機を操作](docs/keyboard_ship.md)
- [当たり判定](docs/hittest.md)

## 応用サンプル

- [アナログ時計を作ろう](docs/clock.md)
- [物理演算でボールを作ろう](docs/ball.md)
- [フラクタル図形](docs/fractal.md)

## その他

- [スマホブラウザで全画面に表示する対応](docs/fullscreen.md)
- [Adobe Flash Professional CC (Animate CC)とCreateJSの連携](docs/adobe_animate.md)
