# Adobe Flash Professional CC (Animate CC)とCreateJSの連携

<iframe width="560" height="315" src="https://www.youtube.com/embed/RK6Z-ExOwuw" frameborder="0" allowfullscreen></iframe>

Flash Professional CCからHTML5 Canvas素材として書き出し、CreateJSで利用する手順を紹介します。[YouTubeのチュートリアル動画](https://www.youtube.com/watch?v=RK6Z-ExOwuw)を参考に進めましょう。

1. Flash Professional CC (Flash Pro CC)を起動
2. シンボルを作成する (ここでは「Star」という名前で作成)
3. Flash Pro CCで[プレビュー]するとHTML5素材が出力される
4. 別ファイルとしてHTMLファイルを作成
5. CreateJSフレームワークを読み込む
6. CreateJSフレームワークのムービークリップ機能も読み込む (必須)
7. Flash Pro CCから出力したJSファイルも読み込む
8. CreateJS起動のためのコードを記載する
9. Flash Pro CCのシンボルは「lib.Star」という名前で呼び出せる
10. 表示オブジェクトとしてStageに追加して制御しよう



[目次に戻る](../ReadMe.md)
