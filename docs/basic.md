# CreateJSとは / CreateJSのサンプル

## CreateJSとは

[CreateJS](http://www.createjs.com)は、HTML5でリッチコンテンツを制作するためのJavaScriptライブラリのスイート（特定用途のソフトウェアを詰め合わせたパッケージ）です。Flashデベロッパーとして著名なGrant Skinner氏が開発を行っており、オープンソースソフトウェアとして個人・商用でも無償で利用できます。

CreateJSには、リッチコンテンツを制作するための複数のJavaScriptライブラリが含まれています。

- EaselJS	: HTML5 Canvasでの制作を扱うためのソリューションが提供されています。
- TweenJS	: Javascriptで使用するためのシンプルなトゥイーンライブラリです。
- SoundJS	: 簡単にサウンドを利用できるライブラリです。
- PreloadJS	: 素材（画像、音声、JS、データ）を先読みできるライブラリです。

## CreateJSのサンプル

- [gskinner lab](http://lab.gskinner.com)
- [Planetary Gary](http://sandbox.createjs.com/PlanetaryGary/)
- [CreateJS Demo](http://www.createjs.com/#demos)
- [日本全国花粉飛散マップ](http://ics-web.jp/projects/pollenmap/)
- [jsdo.it](http://jsdo.it/tag/createjs?search_order=favorite)

[目次に戻る](../ReadMe.md)
